NPMI?=npm i
NPX?=npx
all:
	@echo possible targets: dist dev html serve clean realclean tree
	@exit 1
dist: node_modules
	${NPX} vite build src --base=/$@ --outDir=../$@
dev:: node_modules
	${NPX} vite  dev  src --base=/$@
serve:: html
	python -mhttp.server -dhtml 8000
html: node_modules
	cp -r src $@
	${NPX} tailwindcss -mo $@/site.css
node_modules: package.json
	${NPMI}
package.json:
	${NPMI} tailwindcss @tailwindcss/typography autoprefixer daisyui vite
tree::;tree -I .git -asFp
_clean::
	find . -name  \*~ -o -name .\*~ | xargs rm -fr
	find . -name html -o -name dist | xargs rm -fr
_realclean::
	rm -fr bun.lockb packa* node_modules auto-save-list
clean::     _clean            tree
realclean:: _clean _realclean tree
