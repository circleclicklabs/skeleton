/** @type {import('tailwindcss').Config} */
module.exports = {
    content: {
	relative: true,
	files: [
	       "src/**/*.{html,js}",
	      "user/**/*.{html,js}",
	    "public/**/*.{html,js}",
	],
    },
    plugins: [
	require("@tailwindcss/typography"),
	require("daisyui"),
    ],
    theme: {},
}
